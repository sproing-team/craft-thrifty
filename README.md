# Thrifty

This is an internal standardized plugin to be used for all Craft websites. It is to be installed on a fresh Craft installation and is not tested for backwards compatibility.

## Installation

1. Upload the `thrifty` directory to `craft/plugins/` on your server.
2. Enable the plugin under Craft Admin > Settings > Plugins

## Components

Cocktail Recipes provides examples of the following Craft components:

- Menus
- SEO

## SEO

### Meta Priority
1. Custom Template Overrides
2. Entry Fields
3. Global Fields

#### Managing your SEO metadata in your templates

First, let’s add one line of code to the `<head>` tag in the header of our templates where we want our metadata to be placed.

```
{% block meta %}
    {{ craft.thrifty.meta() }}
{% endblock %}
```
You can override the block on individual templates with custom values.

```
{% block meta %}
    {{ craft.thrifty.meta({
        title: ''
        desc: '',
        keywords: '',
        shareImageUrl: '',
        facebookPageUrl: '',
        twitterUsername: '',
        googlePageUrl: ''
    }) }}
{% endblock %}
```

## Twig Extensions

### Extract

Parses text and highlights the search query.

```
{% set query = craft.request.getParam('q') %}
{% set text = block.text|striptags %}
<p>{{ text|extract(query) }}</p>
```