<?php
namespace Craft;

class ThriftyPlugin extends BasePlugin
{
    public function init()
    {
        if (craft()->request->isCpRequest()) {
            craft()->templates->includeCssResource('thrifty/css/styles-cp.min.css');
            craft()->templates->includeJsResource('thrifty/js/dist/dependencies-cp.min.js', false);

            // Redactor plugins
            craft()->templates->includeCssResource('thrifty/redactor/alignment/alignment.css');
            craft()->templates->includeJsResource('thrifty/redactor/alignment/alignment.js', false);

            craft()->templates->includeJsResource('thrifty/redactor/codemirror/codemirror.js', false);
            craft()->templates->includeJsResource('thrifty/redactor/table/table.js', false);
        }
    }

    public function getName()
    {
        return 'Thrifty';
    }

    public function getDocumentationUrl()
    {
        return 'https://bitbucket.org/sproing-team/craft-thrifty';
    }

    public function getVersion()
    {
        return '1.0.1';
    }

    public function getReleaseFeedUrl()
    {
        return 'https://bitbucket.org/sproing-team/craft-thrifty/raw/master/thrifty/releases.json';
    }

    public function getDeveloper()
    {
        return 'Sproing Creative';
    }

    public function getDeveloperUrl()
    {
        return 'http://sproing.ca';
    }

    protected function defineSettings()
    {
        return [
            'seoGlobalTitle' => [
                AttributeType::String,
                'default' => ''
            ],
            'seoGlobalDesc' => [
                AttributeType::String,
                'default' => ''
            ],
            'seoGlobalKeywords' => [
                AttributeType::String,
                'default' => ''
            ],

            'shareImageUrl' => [
                AttributeType::String,
                'default' => ''
            ],
            'facebookPageUrl' => [
                AttributeType::String,
                'default' => ''
            ],
            'twitterUsername' => [
                AttributeType::String,
                'default' => ''
            ],
            'googlePageUrl' => [
                AttributeType::String,
                'default' => ''
            ]
        ];
    }

    public function hasCpSection()
    {
        return true;
    }

    public function registerCpRoutes()
    {
        return [
            'thrifty' => ['action' => 'thrifty/thriftyIndex'],

            // Menus
            'thrifty/menus' => ['action' => 'thrifty/menus/menuIndex'],
            'thrifty/menus/new' => ['action' => 'thrifty/menus/editMenu'],
            'thrifty/menus/(?P<menuId>\d+)' => ['action' => 'thrifty/menus/editMenu'],
            'thrifty/menus/nodes' => ['action' => 'thrifty/menusNodes/nodeIndex'],
            'thrifty/menus/(?P<menuHandle>{handle})/new' => ['action' => 'thrifty/menusNodes/editNode'],
            'thrifty/menus/(?P<menuHandle>{handle})/(?P<nodeId>\d+)' => ['action' => 'thrifty/menusNodes/editNode'],

            // SEO
            'thrifty/seo' => ['action' => 'thrifty/meta/MetaIndex'],
            'thrifty/social' => ['action' => 'thrifty/meta/SocialIndex']
        ];
    }

    public function registerSiteRoutes()
    {
        return [
            'sitemap.xml' => ['action' => 'thrifty/sitemap/SitemapIndex']
        ];
    }

    public function addTwigExtension()
    {
        Craft::import('plugins.thrifty.twigextensions.ExtractTwigExtension');

        return new ExtractTwigExtension();
    }
}
