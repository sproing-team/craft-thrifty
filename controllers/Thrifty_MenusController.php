<?php
namespace Craft;

/**
 * Menus controller
 */
class Thrifty_MenusController extends BaseController
{
    /**
     * Menu index
     */
    public function actionMenuIndex()
    {
        $variables['menus'] = craft()->thrifty_menus->getAllMenus();

        // Breadcrumbs
        $variables['crumbs'] = [
            [
                'label' => Craft::t('Thrifty'),
                'url' => UrlHelper::getUrl('thrifty')
            ]
        ];

        $this->renderTemplate('thrifty/menus/_index', $variables);
    }

    /**
     * Edit a menu.
     *
     * @param array $variables
     * @throws HttpException
     * @throws Exception
     */
    public function actionEditMenu(array $variables = [])
    {
        $variables['brandNewMenu'] = false;

        if (!empty($variables['menuId'])) {
            if (empty($variables['menu'])) {
                $variables['menu'] = craft()->thrifty_menus->getMenuById($variables['menuId']);

                if (!$variables['menu']) {
                    throw new HttpException(404);
                }
            }

            $variables['title'] = $variables['menu']->name;
        } else {
            if (empty($variables['menu'])) {
                $variables['menu'] = new Thrifty_MenusModel();
                $variables['brandNewMenu'] = true;
            }

            $variables['title'] = Craft::t('Create a new menu');
        }

        $variables['crumbs'] = [
            [
                'label' => Craft::t('Thrifty'),
                'url' => UrlHelper::getUrl('thrifty')
            ],
            [
                'label' => Craft::t('Menus'),
                'url' => UrlHelper::getUrl('thrifty/menus')
            ]
        ];

        $this->renderTemplate('thrifty/menus/_edit', $variables);
    }

    /**
     * Saves a menu
     */
    public function actionSaveMenu()
    {
        $this->requirePostRequest();

        $menu = new Thrifty_MenusModel();

        // Shared attributes
        $menu->id = craft()->request->getPost('menuId');
        $menu->name = craft()->request->getPost('name');
        $menu->handle = craft()->request->getPost('handle');
        $menu->type = craft()->request->getPost('type');
        $menu->maxLevels = craft()->request->getPost('maxLevels');
        $menu->structureId = craft()->request->getPost('structureId');


        // Save it
        if (craft()->thrifty_menus->saveMenu($menu)) {
            craft()->userSession->setNotice(Craft::t('Menu saved.'));
            $this->redirectToPostedUrl($menu);
        } else {
            craft()->userSession->setError(Craft::t('Couldn’t save menu.'));
        }

        // Send the menu back to the template
        craft()->urlManager->setRouteVariables(['menu' => $menu]);
    }

    /**
     * Deletes a menu.
     */
    public function actionDeleteMenu()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $menuId = craft()->request->getRequiredPost('id');

        craft()->thrifty_menus->deleteMenuById($menuId);
        $this->returnJson(['success' => true]);
    }
}
