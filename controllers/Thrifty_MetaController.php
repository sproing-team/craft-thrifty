<?php
namespace Craft;

/**
 * Thrifty controller
 */
class Thrifty_MetaController extends BaseController
{
    public function actionMetaIndex()
    {
        $plugin = craft()->plugins->getPlugin('thrifty');
        $variables['settings'] = $plugin->getSettings();

        $variables['crumbs'] = [
            [
                'label' => Craft::t('Thrifty'),
                'url' => UrlHelper::getUrl('thrifty')
            ]
        ];

        $this->renderTemplate('thrifty/meta/_index', $variables);
    }

    public function actionSaveMeta()
    {
        $this->requirePostRequest();

        $settings = craft()->request->getPost();

        $plugin = craft()->plugins->getPlugin('thrifty');
        $savedSettings = $plugin->getSettings()->getAttributes();
        $newSettingsRow = array_merge($savedSettings, $settings);

        return craft()->plugins->savePluginSettings($plugin, $newSettingsRow);
    }

    public function actionSocialIndex()
    {
        $plugin = craft()->plugins->getPlugin('thrifty');
        $variables['settings'] = $plugin->getSettings();

        $variables['crumbs'] = [
            [
                'label' => Craft::t('Thrifty'),
                'url' => UrlHelper::getUrl('thrifty')
            ]
        ];

        $this->renderTemplate('thrifty/meta/_social', $variables);
    }
}
