<?php
namespace Craft;

/**
 * Thrifty controller
 */
class ThriftyController extends BaseController
{
	/**
	 * Thrifty index
	 */
	public function actionThriftyIndex()
	{
		$this->renderTemplate('thrifty/_index');
	}
}
