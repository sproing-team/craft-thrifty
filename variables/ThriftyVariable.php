<?php
namespace Craft;

class ThriftyVariable
{
    function getMenuNodes($menuHandle)
    {
        $criteria = craft()->elements->getCriteria('Thrifty_MenusNode');
        $criteria->menu = $menuHandle;

        return $criteria;
    }

    function meta($options = [])
    {
        return craft()->thrifty_meta->meta($options);
    }
}