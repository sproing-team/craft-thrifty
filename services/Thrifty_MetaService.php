<?php
namespace Craft;

class Thrifty_MetaService extends BaseApplicationComponent
{
    public function meta($options)
    {
        $plugin = craft()->plugins->getPlugin('thrifty');
        $globalMeta = $plugin->getSettings();

        // Default meta values
        $meta = [
            'title' => $this->_setMetaValue($globalMeta['seoGlobalTitle'], craft()->getSiteName()),
            'desc' => $this->_setMetaValue($globalMeta['seoGlobalDesc']),
            'keywords' => $this->_setMetaValue($globalMeta['seoGlobalKeywords']),
            'shareImageUrl' => $this->_setMetaValue($globalMeta['shareImageUrl']),
            'facebookPageUrl' => $this->_setMetaValue($globalMeta['facebookPageUrl']),
            'twitterUsername' => $this->_setMetaValue($globalMeta['twitterUsername']),
            'googlePageUrl' => $this->_setMetaValue($globalMeta['googlePageUrl'])
        ];

        // Page meta values
        $element = craft()->urlManager->getMatchedElement();

        if ($element && $element->getElementType() == ElementType::Entry) {

            if (craft()->request->getUrl() == '/') {
                $meta['title'] = $this->_setMetaValue($element->seoTitle, $meta['title']);
            } else {
                $meta['title'] = $this->_setMetaValue($element->seoTitle, $element->title . ' - ' . craft()->getSiteName());
            }

            $meta['desc'] = $this->_setMetaValue($element->seoDesc, $meta['desc']);
            $meta['keywords'] = $this->_setMetaValue($element->seoKeywords, $meta['keywords']);
        }

        // Custom template override
        foreach ($options as $key => $value) {
            $meta[$key] = $this->_setMetaValue($options[$key], $meta[$key]);
        }

        // Use template located in plugin dir
        $oldPath = craft()->templates->getTemplatesPath();
        $newPath = craft()->path->getPluginsPath() . 'thrifty/templates';
        craft()->templates->setTemplatesPath($newPath);

        $html = craft()->templates->render('meta/_meta', $meta);

        craft()->templates->setTemplatesPath($oldPath);

        return TemplateHelper::getRaw($html);
    }

    private function _setMetaValue($var, $default = '')
    {
        return $var == '' ? $default : $var;
    }
}