<?php
namespace Craft;

/**
 * Menus - Node model
 */
class Thrifty_MenusNodeModel extends BaseElementModel
{
    protected $elementType = 'Thrifty_MenusNode';

    /**
     * @access protected
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(
            parent::defineAttributes(),
            [
                'menuId' => AttributeType::Number,
                'linkedEntryId' => AttributeType::Number,
                'customUrl' => AttributeType::String,
                'linkedEntryUrl' => AttributeType::String,
                'newParentId' => AttributeType::Number,
                'newWindow' => AttributeType::Bool
            ]
        );
    }

    /**
     * Returns whether the current user can edit the element.
     *
     * @return bool
     */
    public function isEditable()
    {
        return true;
    }

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        $menu = $this->getMenu();

        if ($menu) {
            return UrlHelper::getCpUrl('thrifty/menus/' . $menu->handle . '/' . $this->id);
        }
    }


    /**
     * Returns the nodes's menus.
     *
     * @return Thrifty_MenusModel|null
     */
    public function getMenu()
    {
        if ($this->menuId) {
            return craft()->thrifty_menus->getMenuById($this->menuId);
        }

        return false;
    }

    /**
     * Returns the nodes url
     *
     * @return String|null
     */
    public function getUrl()
    {
        if ($this->linkedEntryId) {
            return '/' . $this->linkedEntryUrl;
        } else {
            return $this->customUrl;
        }
    }

    /**
     * Returns the nodes url
     *
     * @return String|null
     */
    public function getLink()
    {
        if ($this->linkedEntryId) {
            return '/' . $this->linkedEntryUrl;
        } else {
            return $this->customUrl;
        }
    }

    /**
     * Returns the nodes active state
     *
     * @return String|null
     */
    public function getActive()
    {
        $currentUrl = craft()->request->getUrl();
        $linkUrl = $this->getUrl();

        if ($linkUrl != null) {
            if (strpos($currentUrl, $this->getLink()) !== false) {
                return true;
            }
        }

        return false;
    }

    /*
    * Returns the nodes active state
    *
    * @return String|null
    */
    public function getCurrentUrl()
    {
        $currentUrl = craft()->request->getUrl();

        return $currentUrl;
    }
}
