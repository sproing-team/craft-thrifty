<?php
namespace Craft;

/**
 * Menus - Menu record
 */
class Thrifty_MenusRecord extends BaseRecord
{
  /**
   * @return string
   */
  public function getTableName()
  {
    return 'menus';
  }

  /**
   * @access protected
   * @return array
   */
  protected function defineAttributes()
  {
    return [
        'name' => [
            AttributeType::Name,
            'required' => true
        ],
        'handle' => [
            AttributeType::Handle,
            'required' => true
        ],
        'maxLevels' => AttributeType::Number,
        'type' => AttributeType::String,
        'structureId' => AttributeType::Number
    ];
  }

  /**
   * @return array
   */
  public function defineRelations()
  {
    return [
        'nodes' => [
            static::HAS_MANY,
            'Menus_NodeRecord',
            'nodeId'
        ],
    ];
  }

  /**
   * @return array
   */
  public function defineIndexes()
  {
    return [
        [
            'columns' => ['name'],
            'unique' => true
        ],
        [
            'columns' => ['handle'],
            'unique' => true
        ],
    ];
  }

  /**
   * @return array
   */
  public function scopes()
  {
    return [
        'ordered' => ['order' => 'name'],
    ];
  }
}
