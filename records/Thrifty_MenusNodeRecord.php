<?php
namespace Craft;

/**
 * Events - Event record
 */
class Thrifty_MenusNodeRecord extends BaseRecord
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'menus_nodes';
    }

    /**
     * @access protected
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'linkedEntryId' => [
                AttributeType::Number,
                'required' => false
            ],
            'customUrl' => [
                AttributeType::String,
                'required' => false
            ],
            'newWindow' => [
                AttributeType::Bool,
                'required' => false
            ]
        ];
    }

    /**
     * @return array
     */
    public function defineRelations()
    {
        return [
            'element' => [
                static::BELONGS_TO,
                'ElementRecord',
                'id',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'menu' => [
                static::BELONGS_TO,
                'Thrifty_MenusRecord',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'linkedEntry' => [
                static::HAS_ONE,
                'EntryRecord',
                'linkedEntryId',
                'required' => false
            ],
        ];
    }
}
