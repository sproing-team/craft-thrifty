<?php
namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class ExtractTwigExtension extends \Twig_Extension
{
    private $digitToWord = [
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
    ];

    public function getName()
    {
        return 'extract';
    }

    public function getFilters()
    {
        return [
            'extract' => new Twig_Filter_Method($this, 'extract'),
        ];
    }

    public function extract($text, $query)
    {
        //words
        $words = join('|', explode(' ', preg_quote($query)));

        //lookahead/behind assertions ensures cut between words
        $s = '\s\x00-/:-@\[-`{-~'; //character set for start/end of words
        preg_match_all('#(?<=[' . $s . ']).{1,30}((' . $words . ').{1,30})+(?=[' . $s . '])#uis', $text, $matches, PREG_SET_ORDER);

        //delimiter between occurences
        $results = [];
        foreach ($matches as $line) {
            $results[] = htmlspecialchars($line[0], 0, 'UTF-8');
        }
        $result = join(' <strong>(...)</strong> ', $results);

        //highlight
        $result = preg_replace('#' . $words . '#iu', "<span class=\"highlight\">\$0</span>", $result);

        return TemplateHelper::getRaw($result);
    }
}