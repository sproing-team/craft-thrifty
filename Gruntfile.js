module.exports = function (grunt) {
	// Project Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			sass: {
				files: ['resources/css/sass/*.scss'],
				tasks: 'sass:dist'
			}
		},
		concat: {
			options: {
				separator: ';'
			},
			dependencies: {
				src: [
					'bower_components/codemirror/lib/codemirror.js'
				],
				dest: 'resources/js/dist/dependencies-cp.min.js'
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
				preserveComments: false
			},
			dependencies: {
				src: 'resources/js/dist/dependencies-cp.min.js',
				dest: 'resources/js/dist/dependencies-cp.min.js'
			}
		},
		sass: {
			options: {
				style: 'compressed',
				sourcemap: 'none'
			},
			dist: {
				files: {
					'resources/css/styles-cp.min.css': 'resources/css/sass/styles-cp.scss'
				}
			}
		}
	});

	// Load tasks
	require('load-grunt-tasks')(grunt);

	// Public tasks
	grunt.registerTask('default', [
		'sass',
		'concat',
		'uglify',
		'watch'
	]);
};